<?php 

$ambil = tampildataproduk();

if (isset($_POST['simpanbarangkeluar']))
{
    tambahbarangkeluar();
}

 $ambilstokkeluar = ambilstokkeluar();
 $ambilbarangkeluar =  ambilbarangkeluar();

?>

<div class = "content">
      <div class = "panel is-primary">
          <p class = "barang">barang Keluar</p>
          <h3>Informasi barang Masuk Dari PT Izu TokuFans Indonesia</h3>
          <button type = "submit" class = "button is-primary is-small btn" id = "openmodal">Tambah data</button>
            <a href="exportbarangkeluarexel.php" target="_blank" class = "button is-info is-small">Download Excell</a>
       
    </div>

    <div class = "hero">

        <div class = "panel is-primary">
        <table class="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Deskripsi</th>
                    <th>Tanggal keluar</th>
                    <th>Jumlah keluar</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            
            <tbody>
                <?php $no = 1; ?>
                <?php foreach($ambilbarangkeluar as $keluar) :  ?>
                <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $keluar['nama_barang']; ?></td>
                    <td><?php echo $keluar['deskripsi']; ?></td>
                    <td><?php echo $keluar['tanggal_keluar']; ?></td>
                    <td><?php echo $keluar['jumlah']; ?></td>
                    <td>
                         <a href="index.php?halaman=hapusbarangkeluar&id=<?php echo $keluar['id_keluar'] ?>" class = "button is-danger is-small"><i class="fas fa-trash-alt"></i></a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            </table>
        </div>
    </div>
 </div>


 <div class="modal">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Silakan Isi Data</p>
      <button class="delete" aria-label="close" id = "btnclose"></button>
    </header>
    <section class="modal-card-body">
      <!-- Content ... -->
      <form action="" method = "POST">
               <div class = "group">
               <div class="select">
                    <select name = "id_barang" required>
                        <option>Pilih barang -----</option>
                        <?php foreach($ambilstokkeluar as $keluar) : ?>
                        <option value = "<?php echo $keluar['id_barang'] ;?>" required ><?php echo $keluar['nama_barang']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
               </div>
               
               <div class = "group">
                   <label for="">Stok</label>
                    <input class="input " type="text" placeholder="Stok" name = "jumlah">
               </div>
               <button type = "submit" name = "simpanbarangkeluar" class="button is-success mt-4" id = "btn-simpan">Simpan</button>
            </form>
    </section>
    <footer class="modal-card-foot">
     
    </footer>
  </div>
</div>