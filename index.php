<?php 
session_start();

if (empty($_SESSION['login']) OR !isset($_SESSION['login']))
{
    header("Location: login.php");
}
require_once 'proses/proses.php';


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bulma.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300&display=swap" rel="stylesheet">
    <title>Welcome Iventory App</title>
</head>
<body>
    <nav class="navbar" role="navigation" aria-label="main navigation">
        <div class = "container">
            <div class="navbar-brand">
                <!-- <a class="navbar-item" href="https://bulma.io">
                <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28">
                </a> -->
                <a href="" class = "navbar-item">Izu Iventory</a>

                <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                </a>
        </div>

        <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-start">
             <a class="navbar-item" href = "index.php">
                   Home
             </a>


                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href = "index.php">
                        Iventory
                    </a>

                    <div class="navbar-dropdown">
                    <a class="navbar-item" href = "index.php?halaman=stokbarang">
                        Stok Barang
                    </a>
                    <a class="navbar-item" href = "index.php?halaman=barangmasuk">
                        barang Masuk
                    </a>
                    <a class="navbar-item" href = "index.php?halaman=barangkeluar">
                        Barang Keluar
                    </a>
                    
                    </div>
                </div>

                 <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link">
                        Akun
                    </a>

                    <div class="navbar-dropdown">
                    <a class="navbar-item" href = "index.php?halaman=gantipassword">
                        Ganti Password
                    </a>
                   
                 
                    
                    </div>
                </div>
            </div>

            <div class="navbar-end">
            <div class="navbar-item">
                <div class="buttons">
                
               <?php if (isset($_SESSION['login'])) { ?>
                <a class="button is-primary" href = 'logout.php'>
                    Log out
                </a>
                <?php }else { ?>

                    <a class="button is-primary">
                    Log in
                  </a>
                <?php } ?>
                </div>
            </div>
            </div>
        </div>
        </div>
    </nav>

    <div class = "container">
       <?php 

        if (isset($_GET['halaman']))
        {
            if ($_GET['halaman'] == "stokbarang")
            {
                require_once 'stokbarang.php';
            }
            else if ($_GET['halaman'] == "tambahproduk")
            {
                require_once 'tambahproduk.php';
            }
            else if ($_GET['halaman'] == "barangmasuk")
            {
                require_once 'barangmasuk.php';
            }
            else if ($_GET['halaman'] == "barangkeluar")
            {
                require_once 'barangkeluar.php';
            }
            else if ($_GET['halaman'] == "hapusstokbarang")
            {
                require_once 'hapusstokbarang.php';
            }
            else if ($_GET['halaman'] == "editstokbarang")
            {
                require_once 'editstokbarang.php';
            }
            else if ($_GET['halaman'] == 'hapusbarangmasuk')
            {
                require_once 'hapusbarangmasuk.php';
            }
            else if ($_GET['halaman'] == 'editbarangmasuk')
            {
                require_once 'editbarangmasuk.php';
            }
            else if ($_GET['halaman'] == 'hapusbarangkeluar')
            {
                require_once 'hapusbarangkeluar.php';
            }
            else if ($_GET['halaman'] == 'gantipassword')
            {
                require_once 'gantipassword.php';
            }
           
            

        }else {
            require_once 'home.php';
        }
       
       ?>

       
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script>
          $("#openmodal").click(function(){
              $('.modal').addClass('is-active');
          });

          

          $('#btn-simpan').click(function(){
              $('.modal').addClass('is-active');
          })

          $('#btnclose').click(function() {
              $('.modal').removeClass('is-active');
          })
       </script>
</body>
</html>