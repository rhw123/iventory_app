<?php 

$id = $_GET['id'];

$resultrow = tampildatastokbarang("SELECT * FROM stok_barang WHERE id_barang = $id");

if (isset($_POST['editstokbarang']))
{
    $simpan = editstokbarang($id);
    if ($simpan)
    {
    	echo "<script>alert('data Berhasil Di Ubah')</script>";
    	echo "<script>location='index.php?halaman=stokbarang'</script>";
    }
    else
    {
    	echo "<script>alert('data gagal Di Ubah')</script>";
    	echo "<script>location='index.php?halaman=stokbarang'</script>";
    }
}

 ?>

 <div class="content">
 	<div class="panel pt-2"> 
 		<button type="submit" class="button is-primary is-small btn" id="openmodal">Edit Sekarang</button>
 		<a href="index.php?halaman=stokbarang" class="button is-info is-small">Kembali</a>
 	</div>
 </div>


  <div class="modal">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Silakan Isi Data</p>
      <button class="delete" aria-label="close" id = "btnclose"></button>
    </header>
    <section class="modal-card-body">
      <!-- Content ... -->
      <form action="" method = "POST">
               <div class = "group">
                   <label for="">Nama Barang</label>
                    <input class="input " type="text" placeholder="Nama Barang" name = "nama_barang" value="<?php echo $resultrow[0]['nama_barang'] ?>" required>
               </div>
               <div class = "group">
                   <label for="">Deskripsi</label>
                    <input class="input " type="text" placeholder="Deskripsi" name = "deskripsi" value="<?php echo $resultrow[0]['deskripsi'] ?>" required>
               </div>
               
               <button type = "submit" name = "editstokbarang" class="button is-success mt-4" id = "btn-simpan">Simpan Perubahan</button>
            </form>
    </section>
    <footer class="modal-card-foot">
     
    </footer>
  </div>
</div>