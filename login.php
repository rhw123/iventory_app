<?php 
session_start();
require_once 'proses/proses.php';

if (isset($_SESSION['login']) OR !empty($_SESSION['login']))
{
    header("Location: index.php");
}

if (isset($_POST['login']))
{
     login();

    
    
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bulma.min.css">
    <link rel="stylesheet" href="css/mycss.css">
    <title>Halaman Login</title>
</head>
<body>

    <div class="container mt-5" style='width:40%;' id = "container">
    <article class="panel is-primary">
        <p class="panel-heading">
           Silakan Login
        </p>
       
       <form action="" method = "POST">
            <div class="panel-block">
                    <div class = "control">
                            <label class="label">Username</label>
                            <div class="control">
                                <input class="input" type="text" placeholder="input username" name = "username" requred>
                            </div>
                    </div>
            </div>
            <div class="panel-block">
                    <div class = "control">
                            <label class="label">Password</label>
                            <div class="control">
                                <input class="input" type="password" placeholder="input password" name = "password" required>
                            </div>
                    </div>
            </div>
                <div class="panel-block">
                     <div class = "control">
                        <button type = "submit" name = "login" class = "button is-info is-small">Masuk Aplikasi</button>
                        <a href="registrasi.php" class = "button button is-warning is-small">Registrasi</a>
                     </div>
                </div>
         </form>
        
    </article>
    </div>
    
</body>
</html>