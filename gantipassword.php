

 <div class="container mt-5" style='width:40%;' id = "container">
    <article class="panel is-primary">
       
       <form action="" method = "POST">
           
            <div class="panel-block">
                    <div class = "control">
                            <label class="label">Password</label>
                            <div class="control">
                                <input class="input" type="password" placeholder="input password" name = "password" required>
                            </div>
                    </div>
            </div>

            <div class="panel-block">
                    <div class = "control">
                            <label class="label">Password Baru</label>
                            <div class="control">
                                <input class="input" type="password" placeholder="input password" name = "passwordbaru" required>
                            </div>
                    </div>
            </div>
                <div class="panel-block">
                     <div class = "control">
                        <button type = "submit" name = "reset" class = "button is-info is-small">Simpan</button>
                        <a href="registrasi.php" class = "button button is-warning is-small">Kembali</a>
                     </div>
                </div>
         </form>
        
    </article>
    </div>

    <?php 

    	if (isset($_POST['reset'])) 
    	{
    		require_once 'proses/proses.php';
    		resetpassword();
    	}

     ?>