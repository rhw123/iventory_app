<?php 

$ambil = tampildataproduk();

if (isset($_POST['simpanbarangmasuk']))
{
    tambahbarangmasuk();
}

 $ambilstokmasuk = ambilstokmasuk();
 $ambilbarangmasuk =  ambilbarangmasuk();

?>

<div class = "content">
      <div class = "panel is-primary">
          <p class = "barang">barang Masuk</p>
          <h3>Informasi barang Masuk Dari PT Izu TokuFans Indonesia</h3>
          <button type = "submit" class = "button is-primary is-small btn" id = "openmodal">Tambah data</button>
          <a href="exportbarangmasukexel.php" target="_blank" class = "button is-info is-small">Download Excell</a>
    </div>

    <div class = "hero">

        <div class = "panel is-primary">
        <table class="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Deskripsi</th>
                    <th>Tanggal Masuk</th>
                    <th>Jumlah Masuk</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            
            <tbody>
                <?php $no = 1; ?>
                <?php foreach($ambilbarangmasuk as $masuk) :  ?>
                <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $masuk['nama_barang']; ?></td>
                    <td><?php echo $masuk['deskripsi']; ?></td>
                    <td><?php echo $masuk['tanggal_masuk']; ?></td>
                    <td><?php echo $masuk['jumlah']; ?></td>
                    <td>
                         <a href="index.php?halaman=hapusbarangmasuk&id=<?php echo $masuk['id_masuk'] ?>" class = "button is-danger is-small"><i class="fas fa-trash-alt"></i></a>
                        <a href="index.php?halaman=editbarangmasuk&id=<?php echo $masuk['id_masuk'] ?>" class = "button is-info is-small"><i class="fas fa-pen-alt"></i></a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            </table>
        </div>
    </div>
 </div>


 <div class="modal">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Silakan Isi Data</p>
      <button class="delete" aria-label="close" id = "btnclose"></button>
    </header>
    <section class="modal-card-body">
      <!-- Content ... -->
      <form action="" method = "POST">
               <div class = "group">
               <div class="select">
                    <select name = "id_barang" required>
                        <option>Pilih barang -----</option>
                        <?php foreach($ambilstokmasuk as $masuk) : ?>
                        <option value = "<?php echo $masuk['id_barang'] ;?>" required ><?php echo $masuk['nama_barang']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
               </div>
               
               <div class = "group">
                   <label for="">Stok</label>
                    <input class="input " type="text" placeholder="Stok" name = "jumlah">
               </div>
               <button type = "submit" name = "simpanbarangmasuk" class="button is-success mt-4" id = "btn-simpan">Simpan</button>
            </form>
    </section>
    <footer class="modal-card-foot">
     
    </footer>
  </div>
</div>