<?php 

$ambil = tampildataproduk();

if (isset($_POST['simpan']))
{
    tambahproduk();
}

?>

<div class = "content">
      <div class = "panel is-primary">
          <p class = "barang">Stok barang</p>
          <h3>Informasi Stok barang Dari PT Izu TokuFans Indonesia</h3>
          <button type = "submit" class = "button is-primary is-small btn" id = "openmodal">Tambah data</button>
          <a href="exportstokbarangexel.php" target="_blank" class = "button is-info is-small">Download Excell</a>
    </div>

    <div class = "hero">

        <div class = "panel is-primary">
        <table class="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                   
                    <th>Deskripsi</th>
                    <th>Stok</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            
            <tbody>
                <?php $no = 1; ?>
                <?php foreach($ambil as $amb) : ?>
                <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $amb['nama_barang'] ?></td>
                    <td><?php echo $amb['deskripsi']; ?></td>
                    <td><?php echo $amb['stok']; ?></td>
                    <td>
                        <a href="index.php?halaman=hapusstokbarang&id=<?php echo $amb['id_barang']; ?>" class = "button is-danger is-small"><i class="fas fa-trash-alt"></i></a>
                        <a href="index.php?halaman=editstokbarang&id=<?php echo $amb['id_barang'] ?>" class = "button is-info is-small"><i class="fas fa-pen-alt"></i></a>
                    </td>
                </tr>
                <?php endforeach; ?>
                     
            </tbody>
            </table>
        </div>
    </div>
 </div>


 <div class="modal">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Silakan Isi Data</p>
      <button class="delete" aria-label="close" id = "btnclose"></button>
    </header>
    <section class="modal-card-body">
      <!-- Content ... -->
      <form action="" method = "POST">
               <div class = "group">
                   <label for="">Nama Barang</label>
                    <input class="input " type="text" placeholder="Nama Barang" name = "nama_barang" required>
               </div>
               <div class = "group">
                   <label for="">Deskripsi</label>
                    <input class="input " type="text" placeholder="Deskripsi" name = "deskripsi" required>
               </div>
               <div class = "group">
                   <label for="">Stok</label>
                    <input class="input " type="text" placeholder="Stok" name = "stok" required>
               </div>
               <button type = "submit" name = "simpan" class="button is-success mt-4" id = "btn-simpan">Simpan</button>
            </form>
    </section>
    <footer class="modal-card-foot">
     
    </footer>
  </div>
</div>



