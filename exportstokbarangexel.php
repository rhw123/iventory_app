               <?php 

               require_once 'proses/proses.php';
               $ambil = tampildataproduk();

               header("Content-type: application/vnd-ms-excel");
               header("Content-Disposition: attachment; filename=Data stok barang.xls");

                ?>

                 <p>Stok Barang</p>
                <h3>Informasi Stok Barang Dari PT Izu TokuFans Indonesia</h3>

                <table>
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>                        
                            <th>Deskripsi</th>
                            <th>Stok</th>
                        
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($ambil as $amb) : ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td><?php echo $amb['nama_barang'] ?></td>
                            <td><?php echo $amb['deskripsi']; ?></td>
                            <td><?php echo $amb['stok']; ?></td>
                        </tr>
                        <?php endforeach; ?>
                             
                    </tbody>
            </table>